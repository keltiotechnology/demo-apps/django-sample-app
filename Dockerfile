# Global ARG, can be either pip or pipenv
ARG BUILD_ENV=pip

# Stage 1: Install system packages (using apt)
ARG PYTHON_VERSION=3.7.11-slim
FROM python:${PYTHON_VERSION} as prepare-image
ENV PYTHONUNBUFFERED 1
# We need these for pycrypto and psycopg2
RUN apt-get update -y \ 
    && apt-get install --no-install-recommends -y gcc libc6-dev libpq-dev libffi-dev python3-dev

# Stage 2a: Install python packages (using pip)
# Note:
# These instructions are triggered when ARG BUILD_ENV=pip
# They will install packages defined in requirements.txt using Pip tool
FROM prepare-image as pip-image
ONBUILD WORKDIR /code
ONBUILD COPY requirements.txt /code
ONBUILD RUN pip install --no-warn-script-location --user -r requirements.txt

# Stage 2b: Install python packages (using pipenv)
# Note:
# These instructions are triggered when ARG BUILD_ENV=pipenv
# They will install packages defined in Pipfile using Pipenv tool
FROM prepare-image as pipenv-image
ONBUILD WORKDIR /code
ONBUILD ENV PIPENV_VENV_IN_PROJECT=1
ONBUILD ENV PATH=/root/.local/bin:$PATH
ONBUILD COPY Pipfile* /code/
ONBUILD RUN pip install --no-warn-script-location --user pipenv
ONBUILD RUN pipenv install

# Stage 3: Actually install python packages
# Produce child image with Python suitable packaging tool depending on global ARG value - BUILD_ENV
FROM ${BUILD_ENV}-image AS build-image

# Stage 4: Produce final image to be used
FROM python:${PYTHON_VERSION}
# Copy value of global ARG and use it as environment variable
ARG BUILD_ENV
ENV BUILD_ENV=$BUILD_ENV
# Copy pip installation result. As we use --user option, they are store in user home's .local directory
COPY --from=build-image /root/.local /root/.local
ENV PATH=/root/.local/bin:$PATH
# Copy pipenv installation result, which is folder venv located in project directory (/code)
COPY --from=build-image /code /code
# Copy all source code
COPY . /code/
WORKDIR /code
# Run script to execute gunicorn web server
# Optionally
# CMD ["sh", "docker_run_script.sh"] # Run script with no arguments. The script uses default values set in the script
CMD ["sh", "docker_run_script.sh", "-b", "0.0.0.0:80", "-m", "django_sample_app.wsgi:application"] # Run script with arguments