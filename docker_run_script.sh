#!/usr/bin/env sh

#######################################
# Program Arguments
#######################################
usage()
{
cat << EOF
This script is gunicorn wrapper. We use this to call gunicorn inside docker container.
usage: $0 [-shv]
OPTIONS:
   | -h     Show this message
   | -m     Provide django project's WSGI module e.g. django_sample_app.wsgi:application
   | -b     Provide gunicorn socket to bind e.g. 0.0.0.0:80
EXAMPLES:
   | docker_run_script.sh -b 0.0.0.0:80 -m django_sample_app.wsgi:application
EOF
}

while getopts "b:m:" option; do
  case $option in
    b ) GUNICORN_SOCKET=$OPTARG
    ;;
    m ) WSGI_MODULE=$OPTARG
    ;;
  esac
done

if [ -z "$GUNICORN_SOCKET" ] && [ -z "$WSGI_MODULE" ]
then
      echo "No argument provided. Setting default values.."
      GUNICORN_SOCKET="0.0.0.0:80"
      WSGI_MODULE="django_sample_app.wsgi:application"
      echo GUNICORN_SOCKET=$GUNICORN_SOCKET
      echo WSGI_MODULE=$WSGI_MODULE
fi

if [ -x "$(command -v pipenv)" ]; then
  exec pipenv run gunicorn --bind $GUNICORN_SOCKET $WSGI_MODULE  
elif [ -x "$(command -v gunicorn)" ]; then
  exec gunicorn --bind $GUNICORN_SOCKET $WSGI_MODULE
else
  echo "Neither pipenv or gunicorn is found in $PATH. Program exits.."
  exit -1
fi