# Django sample app

### Run server for development

```bash
python manage.py runserver
```

### Build with docker

#### 1. Pipenv

If you use pipenv, ensure your project have Pipfile and Pipfile.lock.

To build docker image, provide arg ```BUILD_ENV=pipenv``` to dockerize your code with Pipenv.

```bash
docker build -t django:pipenv --build-arg BUILD_ENV=pipenv .
```

#### 2. Pip

If you just use pip, ensure to have requirements.txt file in the project.

To build docker image, provide arg ```BUILD_ENV=pip``` to dockerize your code with Pip.

```bash
docker build -t django:pip --build-arg BUILD_ENV=pip .
```

#### 3. Python version

To specify python version,we can use arg ```PYTHON_VERSION```.

If you use pipenv, please ensure you change the value of ```python_version``` to target version in Pipfile and Pipfile.lock.
Then you can build the docker image using arg ```PYTHON_VERSION```.


```bash
docker build -t django:pip --build-arg BUILD_ENV=pipenv --build-arg PYTHON_VERSION=3.8.0-slim .
```

### Run docker container

```bash
docker container run --rm -p 80:80 django:pipenv
```
